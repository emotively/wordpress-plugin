<?php
/**
 * Plugin Name: Emotively
 * Plugin URI: http://emotively.net
 * Description: Adds the Emotively User Satisfaction Rating Widget and Testimonial Display Widget to Wordpress sites. Visit emotively.net to create an account.
 * Version: 1.0.0
 * Author: Emotively
 * Author URI: http://emotively.net
 * License: Commercial
 */

add_action( 'wp_enqueue_scripts', 'emotively_enqueued_assets' );

function emotively_enqueued_assets() {
	wp_register_script( 'emotively-rating-widget', '//localhost:3000/scripts/rating-widget.js' );
	wp_register_script( 'emotively-display-widget', '//localhost:3000/scripts/display-widget.js' );

	wp_localize_script(
	  'emotively-rating-widget', 'emotively_rating_widget_options',
	  array('account_id' => esc_attr( get_option('account_id') ))
	);

	wp_localize_script(
	  'emotively-display-widget', 'emotively_display_widget_options',
	  array('account_id' => esc_attr( get_option('account_id') ))
	);


	wp_enqueue_script( 'emotively-rating-widget');
	wp_enqueue_script( 'emotively-display-widget');
}

add_action('admin_menu', 'emotively_menu');

function emotively_menu() {
	add_menu_page('Emotively Plugin Settings', 'Emotively Plugin Settings', 'administrator', 'emotively-display-settings', 'emotively_settings_page', 'dashicons-admin-generic');
}
	
function emotively_settings_page() {

?>
<div class="wrap">
<h2>Emotively Plugin Configuration</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'emotively-settings-group' ); ?>
    <?php do_settings_sections( 'emotively-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Account Identifier</th>
        <td><input type="text" name="account_id" value="<?php echo esc_attr( get_option('account_id') ); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>

<?php
}

add_action( 'admin_init', 'emotively_settings' );

function emotively_settings() {
	register_setting( 'emotively-settings-group', 'account_id' );
}

function shortcode_func( $atts ) {
	return "<div class='emotively-widget-single-view'></div>";
}
add_shortcode( 'emotively_display_single', 'shortcode_func' );

?>